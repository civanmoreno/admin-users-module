<?php

namespace Drupal\admin_users\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the admin_users module.
 */
class AdminUsersControllerTest extends WebTestBase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "admin_users AdminUsersController's controller functionality",
      'description' => 'Test Unit for module admin_users and controller AdminUsersController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests admin_users functionality.
   */
  public function testAdminUsersController() {
    // Check that the basic functions of module admin_users.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
