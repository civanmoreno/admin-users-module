<?php

namespace Drupal\admin_users\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CreateUsersForm.
 */
class CreateUsersForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_users_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result"></div>',
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Nombre'),
      '#maxlength' => 100,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['dni'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identificación'),
      '#maxlength' => 100,
      '#size' => 64,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['date'] = [
      '#type' => 'date',
      '#title' => $this->t('Fecha'),
      '#weight' => '0',
      '#description' => '<span>' . $this->t('Seleccione la fecha de nacimiento') . '</span>',
    ];
    $form['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Cargo'),
      '#options' => ['Administrador' => $this->t('Administrador'), 'Webmaster' => $this->t('Webmaster'), 'Desarrollador' => $this->t('Desarrollador')],
      '#size' => 3,
      '#weight' => '0',
    ];
    /*$form['actions'] = [
      '#type' => 'button',
      '#value' => $this->t('Agregar'),
      '#ajax' => [
        'callback' => '::addUser',
      ],
    ];*/
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enviar'),
      '#ajax' => [
        'callback' => '::addUser',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function addUser(array &$form, FormStateInterface $form_state) {
    $dni = $form_state->getValue('dni');
    if (!is_numeric($dni)) {
      $response = new AjaxResponse();
      $response->addCommand(
        new HtmlCommand(
          '.result',
          'El dni solo permite números')
      );
      return $response;
    }else{
      $state = $form_state->getValue('position') == 'Administrador' ? 1 : 0;
      $data = [
        'name' => $form_state->getValue('name'),
        'dni' => $form_state->getValue('dni'),
        'date' => strtotime($form_state->getValue('date')),
        'position' => $form_state->getValue('position'),
        'estado' => $state,
      ];
      \Drupal::database()->insert('example_users')->fields($data)->execute();
      $response = new AjaxResponse();
      $response->addCommand(
        new HtmlCommand(
          '.result',
          'Registro insertado')
      );
      return $response;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
