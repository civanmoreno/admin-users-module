<?php

namespace Drupal\admin_users\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class AdminUsersController.
 */
class AdminUsersController extends ControllerBase {

  /**
   * Showdata.
   *
   * @return string
   *   Return Hello string.
   */
  public function showData() {
    $result = \Drupal::database()->select('example_users', 'n')
      ->fields('n', ['name', 'dni', 'date', 'position', 'estado'])
      ->execute()->fetchAllAssoc('dni');
    $rows = [];
    foreach ($result as $row => $content) {
      $rows[] = [
        'data' => [$content->name, $content->dni, date('Y-m-d H:i:s', $content->date), $content->position, $content->estado],
      ];
    }
    $header = ['Nombre', 'Identificación', 'Fecha', 'Cargo', 'Estado'];
    $output = [
    // Here you can write #type also instead of #theme.
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
    return $output;
  }

}
